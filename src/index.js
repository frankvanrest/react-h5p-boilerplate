import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';


// use window['H5P'] instead of H5P.MyApp to prevent webpack to change variable names
// in the build process
window['H5P'].MyApp = (function ($) {
  /**
   * Constructor function.
   */
   function C(options, id) {
    // Extend defaults with provided options
    this.options = $.extend(true, {}, {
      greeting: 'Hello world!',
      image: null
    }, options);
    // Keep provided id.
    this.id = id;
  };
 
  /**
   * Attach function called by H5P framework to insert H5P content into
   * page
   *
   * @param {jQuery} $container
   */
  C.prototype.attach = function ($container) {
    // make the H5P container DOM element ($container[0]) the root of the React App 
    const root = ReactDOM.createRoot($container[0]);
    // pass options and the H5P identifier to the React App
    root.render(
      <React.StrictMode>
        <App options={this.options} h5pid={this.id}/>
      </React.StrictMode>
    );
  };
  return C;
  // use window['H5P'] instead of H5P.MyApp to prevent webpack to change variable names
  // in the build process
})(window['H5P'].jQuery);



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
