import './App.css';

function App({options, h5pid}) {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          {options.greeting}
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <img src={window['H5P'].getPath(options.image.path, h5pid)} />
      </header>
    </div>
  );
}

export default App;
